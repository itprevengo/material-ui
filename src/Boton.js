import React from 'react';
import CoolButton from "./CoolButton"

function App() {
    const cool=true;
    return (
        <CoolButton cool={cool}></CoolButton>
    );
}

export default App;
