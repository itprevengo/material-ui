import React from 'react';
import './styles.css';
import Nav from "./Nav";
import About from "./About";
import Shop from "./Shop";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

function App() {
    return (
        <Router>
            <div className="App">
                <Nav/>
                <Route path="/"  exact component={Home}></Route>
                <Route path="/about" component={About}></Route>
                <Route path="/shop" component={Shop}></Route>
            </div>
        </Router>
    );
}

const Home= () => (
    <div>
        <h1>Home Page</h1>
    </div>
)

export default App;
