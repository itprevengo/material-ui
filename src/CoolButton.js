import React from "react";
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import classNames from "classnames";

const useStyles = makeStyles(theme => ({
    buttonText: props => {
        return {
            color: props.cool ? "blue":"red",
            [theme.breakpoints.up("sm")]:{
                color: "cyan",
            },
        }
    },
    buttonBackground: props => {
        return{
            backgroundColor: props.cool ? "orange":"yellow"
        }
    }
}));

export default function Hook (props) {
    const classes=useStyles(props);
    return(
        <Button fullWidth className={classNames(classes.buttonText,classes.buttonBackground)}>The button</Button>
    )
}