import React from 'react';
import './App.css';
import { Button,Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    helloThereStyle:{
        fontStyle: 'oblique',
        color: 'red',
        fontsize: '30px',
    },
    buttonStyles:{
        color: "green",
        border: 0,
    }
})

function App() {

    const classes=useStyles();

    return (
        <div className="App">
            <Typography className={classes.helloThereStyle} color="primary" variant="h1">Hello There</Typography>
            <Button className={classes.buttonStyles} color="primary" variant="contained" disabled={false}>This is our first button</Button>
        </div>
    );
}

export default App;
