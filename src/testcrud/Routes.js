import React, { Component } from 'react'
import MaterialTable from 'material-table'
import courses from './data/courses'
// eslint-disable-next-line no-restricted-globals
import Button from '@material-ui/core/Button';
import {makeStyles} from "@material-ui/core/styles";


const state = {

        columns: [
            {
                title: 'Avatar',
                field: 'imageUrl',
                editComponent: props => (
                    <div>
                        <input
                            accept="image/*"
                            id="contained-button-file"
                            className={{display:'none'}}
                            multiple
                            type="file"
                        />
                        <label htmlFor="contained-button-file">
                            <Button variant="contained" color="primary" component="span">
                                Upload
                            </Button>
                        </label>
                    </div>
                ),
                render: rowData => <img src={rowData.imageUrl} style={{width: 40, borderRadius: '50%'}}/>
            },
            { title: 'Nombre', field: 'name' },
            { title: 'Apellido', field: 'surname' },
            { title: 'Nacimiento', field: 'birthYear', type: 'numeric'},
            { title: 'Origen', field: 'birthCity', lookup: { 34: 'İstanbul', 63: 'Şanlıurfa' },},
        ],
        data:courses,
        selectedRow: null
    }

class App extends Component {
    render() {
        return (
            <MaterialTable
                title="Cursos"
                columns={state.columns}
                data={state.data}
                actions={[
                    {
                        icon: 'save',
                        tooltip: 'Guardar Usuario',
                        onClick: (event, rowData) => alert("You saved " + rowData.name)
                    }
                ]}
                editable={{
                    onRowAdd: newData =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    const data = state.data;
                                    data.push(newData);
                                    this.setState({ data }, () => resolve());
                                }
                                resolve()
                            }, 1000)
                        }),
                    onRowUpdate: (newData, oldData) =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    const data = state.data;
                                    const index = data.indexOf(oldData);
                                    data[index] = newData;
                                    this.setState({ data }, () => resolve());
                                }
                                resolve()
                            }, 1000)
                        }),
                    onRowDelete: oldData =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    let data = state.data;
                                    const index = data.indexOf(oldData);
                                    data.splice(index, 1);
                                    this.setState({ data }, () => resolve());
                                }
                                resolve()
                            }, 1000)
                        }),
                }}
                detailPanel={[
                    {
                        tooltip: 'Show Name',
                        render: rowData => {
                            return (
                                <div
                                    style={{
                                        fontSize: 100,
                                        textAlign: 'center',
                                        color: 'white',
                                        backgroundColor: '#43A047',
                                    }}
                                >
                                    {rowData.name}
                                </div>
                            )
                        },
                    },
                    /*{
                        icon: 'account_circle',
                        tooltip: 'Show Surname',
                        render: rowData => {
                            return (
                                <div
                                    style={{
                                        fontSize: 100,
                                        textAlign: 'center',
                                        color: 'white',
                                        backgroundColor: '#E53935',
                                    }}
                                >
                                    {rowData.surname}
                                </div>
                            )
                        },
                    },
                    {
                        icon: 'favorite_border',
                        openIcon: 'favorite',
                        tooltip: 'Show Both',
                        render: rowData => {
                            return (
                                <div
                                    style={{
                                        fontSize: 100,
                                        textAlign: 'center',
                                        color: 'white',
                                        backgroundColor: '#FDD835',
                                    }}
                                >
                                    {rowData.name} {rowData.surname}
                                </div>
                            )
                        },
                    },*/
                ]}
                /*parentChildData={
                    (row, rows) => rows.find(a => a.id === row.parentId)
                }*/
                onRowClick={((evt, selectedRow) => this.setState({ selectedRow }))}
                options={{
                    rowStyle: rowData => ({
                        backgroundColor: (state.selectedRow && state.selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
                    }),
                    selection: false,
                    paging:true,
                    headerStyle: {
                        backgroundColor: '#01579b',
                        color: '#FFF'
                    },
                    actionsColumnIndex: -1,
                    editableColumnIndex: -1,
                    exportButton: true
                }}
                localization={{
                    header:{
                        actions:"Acciones"
                    },
                    toolbar:{
                        searchTooltip:'Buscar',
                        searchPlaceholder:'Buscar',
                        exportTitle:'Exportar',
                        exportName:'Exportar como CSV'
                    },
                    pagination: {
                        labelRowsSelect: 'cursos',
                        labelDisplayedRows: ' {from}-{to} de {count} cursos',
                        firstTooltip: 'Primero',
                        previousTooltip: 'Anterior',
                        nextTooltip: 'Siguiente',
                        lastTooltip: 'Ultimo'
                    },
                    body:{
                        emptyDataSourceMessage: 'Sin cursos que mostrar',
                        addTooltip:'Agregar',
                        editTooltip:'Editar',
                        editRow:{
                            deleteText:'¿Estás seguro de eliminar este curso?',
                            cancelTooltip:'Cancelar',
                            saveTooltip:'Borrar'
                        }
                    },
                }}
            />
        )
    }
}
export default App;
