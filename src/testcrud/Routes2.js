import React from 'react';
import './styles.css';
import {BrowserRouter as Router, Route,Link} from 'react-router-dom';
import Courses from "./Courses/Courses";
import Experts from "./Experts/Experts";
import Students from "./Students/Students";

function App() {
    return (
        <Router>
            <div style={{width: 1000, margin: '0 auto'}}>
                <ul>
                    <li><Link to='/'>Home</Link></li>
                    <li><Link to='/cursos'>Cursos</Link></li>
                    <li><Link to='/expertos'>Expertos</Link></li>
                    <li><Link to='/estudiantes'>Estudiantes</Link></li>
                </ul>

                <hr />

                <Route exact path='/' component={Home} />
                <Route path='/cursos' component={Courses} />
                <Route path='/expertos' component={Experts} />
                <Route path='/estudiantes' component={Students} />
            </div>
        </Router>
    )
}

const Home= () => (
    <div>
        <h1>Home Page</h1>
    </div>
)

export default App;
