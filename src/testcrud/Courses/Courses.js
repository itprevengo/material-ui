import React, {useState} from 'react';
import '../styles.css';
import {Route,Link} from 'react-router-dom';
import Createcourse from "./create";
import Updatecourse from "./update";
import Readcourse from "./read";
import courses from "../data/courses";

function Courses({match}) {
    const cursosdata=courses;
    const [cursos,setCursos]=useState(cursosdata);
    return (
        <div>
            <h1>Cursos</h1>
            <ul>
                <li>
                    <Link to={`${match.url}/create`}>Crear</Link>
                </li>
                <li>
                    <Link to={`${match.url}/update`}>Editar</Link>
                </li>
                <li>
                    <Link to={`${match.url}/read`}>Listar</Link>
                </li>

            </ul>

            <hr />

            <Route path={`${match.path}/create`} component={Createcourse}/>
            <Route path={`${match.path}/update`} component={Updatecourse}/>
            <Route path={`${match.path}/read`} render={props =>
                (<Readcourse {...props} cursos={cursos}/>)
            }/>
        </div>
    )
}
export default Courses;
