import React,{useState} from 'react';
import '../../App.css';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '25ch',
        },
    },
}));

function Createcourse() {
    const classes = useStyles();
    return (
        <div className="App">
            <div>
                <h1>Crear Cursos</h1>
            </div>
            <div>
                <form className={classes.root} noValidate autoComplete="off">
                    <TextField id="name" label="Nombre" variant="outlined" />
                    <TextField id="description" label="Descripccion" variant="outlined" />
                </form>
            </div>
        </div>
    );
}


export default Createcourse;
