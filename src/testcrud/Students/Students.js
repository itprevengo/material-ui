import React from 'react';
import '../styles.css';
import {Route,Link} from 'react-router-dom';
import Createstudent from "./create";
import Updatestudent from "./update";
import Deletestudent from "./delete";

function Courses({match}) {
    return (
        <div>
            <h1>Estudiantes</h1>
            <ul>
                <li>
                    <Link to={`${match.url}/create`}>Crear</Link>
                </li>
                <li>
                    <Link to={`${match.url}/update`}>Editar</Link>
                </li>
                <li>
                    <Link to={`${match.url}/read`}>Listar</Link>
                </li>

            </ul>

            <hr />

            <Route path={`${match.path}/create`} component={Createstudent}/>
            <Route path={`${match.path}/update`} component={Updatestudent}/>
            <Route path={`${match.path}/read`} component={Deletestudent}/>
        </div>
    )
}
export default Courses;
