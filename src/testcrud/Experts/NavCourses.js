import React from 'react';
import '../styles.css';
import {Link} from 'react-router-dom';

function NavCourses() {
    return (
        <nav>
            <h3>Cursos</h3>
            <ul className="nav-links">
                <Link to="/courses/create">
                    <li>Crear</li>
                </Link>
                <Link to="/shop">
                    <li>Listar</li>
                </Link>
            </ul>
        </nav>
    );
}

export default NavCourses;
