import React from 'react';
import '../styles.css';
import {Route,Link} from 'react-router-dom';
import Createexpert from "./create";
import Updateexpert from "./update";
import Deleteexpert from "./delete";

function Courses({match}) {
    return (
        <div>
            <h1>Expertos</h1>
            <ul>
                <li>
                    <Link to={`${match.url}/create`}>Crear</Link>
                </li>
                <li>
                    <Link to={`${match.url}/update`}>Editar</Link>
                </li>
                <li>
                    <Link to={`${match.url}/read`}>Listar</Link>
                </li>

            </ul>

            <hr />

            <Route path={`${match.path}/create`} component={Createexpert}/>
            <Route path={`${match.path}/update`} component={Updateexpert}/>
            <Route path={`${match.path}/read`} component={Deleteexpert}/>
        </div>
    )
}
export default Courses;
