import React from 'react';
import './styles.css';
import {Link} from 'react-router-dom';

function Navcrud() {
    return (
        <nav>
            <h3>Logo</h3>
            <ul className="nav-links">
                <Link to="/courses"><li>Cursos</li></Link>
                <Link to="/students"><li>Estudiantes</li></Link>
                <Link to="/experts"><li>Experts</li></Link>
            </ul>
        </nav>
    );
}

export default Navcrud;
