import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
// import Home from './Home'
//import Boton from './Boton'
// import Home from './testrouter/Home'
import Routes from './testcrud/Routes'
import Routes2 from './testcrud/Routes2'
import * as serviceWorker from './serviceWorker';
import {ThemeProvider} from '@material-ui/core/styles';
import theme from "./theme";
import Nestedroutes from './nestedroutes/nestedroute'
import Muitable from './Muitable'
import Animation from "./animations/Animation";

ReactDOM.render(
  <React.StrictMode>
      <ThemeProvider theme={theme}>
          {/*<App />*/}
          {/*<Home></Home>*/}
          {/*<Boton></Boton>*/}
          {/*<Routes></Routes>*/}
          <Routes2></Routes2>
          {/*<Animation></Animation>*/}
          {/*<Muitable></Muitable>*/}
          {/*<Nestedroutes></Nestedroutes>*/}
      </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
