import React from 'react';
import '../App.css';
import data from "./data";
import {Route,Link} from 'react-router-dom';
import Topiccomponent from "./Topiccomponent";

function Topicscomponent({ match }) {
    return (
        <div>
            <h1>Topics</h1>
            <ul>
                {data.map(({ name, id }) => (
                    <li key={id}>
                        <Link to={`${match.url}/${id}`}>{name}</Link>
                    </li>
                ))}
            </ul>

            <hr />

            <Route path={`${match.path}/:topicId`} component={Topiccomponent}/>
        </div>
    )
}


export default Topicscomponent;
