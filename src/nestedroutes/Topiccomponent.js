import React from 'react';
import '../App.css';
import data from "./data";
import {Route,Link} from 'react-router-dom';
import Resourcecomponent from "./Resourcecomponent";

function Topiccomponent({ match }) {
    const topic = data.find(({ id }) => id === match.params.topicId)
    return (
        <div>
            <h2>{topic.name}</h2>
            <p>{topic.description}</p>

            <ul>
                {topic.resources.map((sub) => (
                    <li key={sub.id}>
                        <Link to={`${match.url}/${sub.id}`}>{sub.name}</Link>
                    </li>
                ))}
            </ul>

            <hr />

            <Route path={`${match.path}/:subId`} component={Resourcecomponent} />
        </div>
    )
}


export default Topiccomponent;
