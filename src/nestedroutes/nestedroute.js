import React, {Component} from "react";
import {BrowserRouter as Router,Route,Link} from 'react-router-dom';
import Homecomponent from "./Homecomponent";
import Topicscomponent from "./Topicscomponent";

class App extends Component {
    render() {
        return (
            <Router>
                <div style={{width: 1000, margin: '0 auto'}}>
                    <ul>
                        <li><Link to='/'>Home</Link></li>
                        <li><Link to='/topics'>Topics</Link></li>
                    </ul>

                    <hr />

                    <Route exact path='/' component={Homecomponent} />
                    <Route path='/topics' component={Topicscomponent} />
                </div>
            </Router>
        )
    }
}

export default App;